using System.Linq;
using System.Net.Mime;
using System.Runtime.Serialization;
using System.Xml.Schema;
using SharpDX.Win32;
using Xenko.Core.Mathematics;
using Xenko.Engine;
using Xenko.Input;
using Xenko.Physics;
using Xenko.UI.Controls;

namespace Glasses
{
public interface IInteraction
{
	string InteractionName { get; }
	void Interact(Player player);
}

public class PlayerInteraction : SyncScript
{
	[DataMember]
	public CameraComponent Camera;
	public UIComponent UIComponent;

	public float Distance = 1.5f;

	private TextBlock text;
	private CharacterComponent character;

	public override void Start()
	{
		character = Entity.Get<CharacterComponent>();
		Game.Window.AllowUserResizing = true;
		text = (TextBlock)UIComponent.Page.RootElement.FindName("E");
		UIComponent.Enabled = false;
	}

	private readonly Vector2 middle = Vector2.One / 2;

	public override void Update()
	{
		UIComponent.Enabled = false;

		var simulation = character.Simulation;
		var cameraTransform = Camera.Entity.Transform;

		var forward = Camera.GetScreenPosInWorld(middle) - cameraTransform.Global().position;
		var cameraPosition = cameraTransform.Global().position;

		var hit = simulation.Raycast(cameraPosition, cameraPosition + forward * Distance);
		if (!hit.Succeeded)
			return;

		var interactions = hit.Collider.Entity.Components.OfType<IInteraction>().ToArray();
		if (!interactions.Any())
			return;

		var interaction = hit.Collider.Entity.Components.OfType<IInteraction>().First();
		
		UIComponent.Enabled = true;
		text.Text = $"Press E to {interaction.InteractionName}";

		if (!Input.IsKeyPressed(Keys.E))
			return;

		interaction.Interact(Entity.Get<Player>());
	}
}
}
