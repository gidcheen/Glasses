using Xenko.Engine;
using Xenko.Input;

namespace Glasses
{
public class MouseLocker : SyncScript
{
	public override void Start()
	{
#if !DEBUG
		Input.LockMousePosition(true);
		Game.IsMouseVisible = false;
#endif
	}

	public override void Update()
	{
//#if DEBUG
		if (Input.IsMouseButtonDown(0))
		{
			Input.LockMousePosition(true);
			Game.IsMouseVisible = false;
		}
		else if (Input.IsKeyPressed(Keys.Escape))
		{
			Input.UnlockMousePosition();
			Game.IsMouseVisible = true;
		}
//#endif
	}
}
}
