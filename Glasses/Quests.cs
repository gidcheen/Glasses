using System;
using System.Collections.Generic;
using System.Linq;
using Xenko.Core;
using Xenko.Engine;
using Xenko.UI.Controls;

namespace Glasses
{
[DataContract]
public struct Quest
{
	public string Name;
	public string Description;
}

public class QuestManager : StartupScript
{
	[DataMember]
	public UIComponent UIComponent = null;

	[DataMember]
	public List<Quest> Open = new List<Quest>();
	[DataMember]
	public List<Quest> Done = new List<Quest>();

	private TextBlock text;

	public event Action OnQuestsCompleted;

	public override void Start()
	{
		text = (TextBlock)UIComponent.Page.RootElement.FindName("Quests");
		UpdateUI();
	}

	public void FinishQuest(string name)
	{
		var quest = Open.Find(q => q.Name == name);

		Open.Remove(quest);
		Done.Add(quest);

		UpdateUI();

		if (!Open.Any())
			OnQuestsCompleted?.Invoke();
	}

	private void UpdateUI()
	{
		text.Text = GetUIText();
		text.Height = (text.Text.Count(c => c == '\n') - 1) * text.ActualTextSize * 1.5f;
	}

	private string GetUIText()
	{
		string ret = null;

		if (Open.Any())
			ret += "Todo:\n";

		foreach (var quest in Open)
			ret += $"- {quest.Description}\n";

		if (Open.Any() && Done.Any())
			ret += "\n";

		if (Done.Any()) 
			ret += "Done:\n";

		foreach (var quest in Done)
			ret += $"{quest.Description}\n";

		return ret;
	}
}
}
