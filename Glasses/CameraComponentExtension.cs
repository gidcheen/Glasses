using Xenko.Core.Mathematics;
using Xenko.Engine;

namespace Glasses
{
public static class CameraComponentExtension
{
	public static Vector3 GetScreenPosInWorld(this CameraComponent camera, Vector2 screenPos, float zPos = 1)
	{
		camera.Entity.Transform.UpdateWorldMatrix();
		camera.Update();
		
		var invViewProj = Matrix.Invert(camera.ViewProjectionMatrix);

		screenPos.X = screenPos.X * 2f - 1f;
		screenPos.Y = 1f - screenPos.Y * 2f;
		var globalPos = Vector3.Transform(new Vector3(screenPos.X, screenPos.Y, 0), invViewProj);
		globalPos /= globalPos.W;

		//var globalPos = camera.Entity.Transform.LocalToWorld(global.XYZ());

		var cameraPos = camera.Entity.Transform.GetGlobalPosition();

		var forward = globalPos.XYZ() - cameraPos;

		return cameraPos + forward * (1 / camera.NearClipPlane);
	}
}
}
