using System;
using System.Collections.Specialized;
using System.Linq;
using Xenko.Audio;
using Xenko.Engine;

namespace Glasses
{
public class Wakeable : SyncScript
{
	public Wakeness Wakeness;
	public Noise Noise;
	public Player Player;
	public float MinDistance = 3.5f;
	public PhysicsComponent Trigger;
	public float Factor = 0.7f;

	public AudioEmitterComponent AudioEmitter;
	private AudioEmitterSoundController scream;

	private NoiseAmount noiseAmount = new NoiseAmount { Value = 0 };

	private bool wasColliding;

	public override void Start()
	{
		scream = AudioEmitter["Scream"];
		scream.IsLooping = false;
		
		Trigger.Collisions.CollectionChanged += (sender, args) =>
		{
			if (args.Action != NotifyCollectionChangedAction.Add)
				return;

			scream.Play();

			if (Trigger.Collisions.SelectMany(c => new[] { c.ColliderA, c.ColliderB }).Any(c => c.Entity == Player.Entity))
				Wakeness.Value = 1;
		};
	}

	public override void Update()
	{
		var distance = Math.Abs((Player.Entity.Transform.Position - Entity.Transform.Position).Length());
		var isColliding = distance < MinDistance;

		if (isColliding != wasColliding)
		{
			if (isColliding)
				Noise.Noises.Add(noiseAmount);
			else
				Noise.Noises.Remove(noiseAmount);
		}
		wasColliding = isColliding;

		noiseAmount.Value = (1 - distance / MinDistance) * Player.NoiseAmount * Factor;
	}
}
}
