using System.Collections.Generic;
using System.Linq;
using Xenko.Core;
using Xenko.Core.Mathematics;
using Xenko.Engine;

namespace Glasses
{
[DataContract]
public class NoiseAmount
{
	[DataMember]
	public double Value = 0.1;
}

public class Noise : SyncScript
{
	[DataMemberIgnore]
	public HashSet<NoiseAmount> Noises { get; } = new HashSet<NoiseAmount>();
	public double Value => MathUtil.Clamp(Noises.Sum(n => n.Value), 0, 1);

	public float Factor { get; set; } = 0.2f;

	public Wakeness Wakeness { get; set; }

	public override void Update()
	{
		Wakeness.Value += Value * Factor * (float)Game.DeltaTime();
	}
}
}
