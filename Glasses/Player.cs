using System.Runtime.Serialization;
using Xenko.Core;
using Xenko.Core.Mathematics;
using Xenko.Engine;
using Xenko.Input;
using Xenko.Physics;

namespace Glasses
{
public class Player : SyncScript
{
	public float Speed { get; set; } = 2;
	public float RunFactor { get; set; } = 2;
	public float Acceleration { get; set; } = 0.1f;
	public TransformComponent CameraRootPith { get; set; }
	public TransformComponent CameraRootYaw { get; set; }

	public CameraComponent Camera { get; set; }

	public float PitchClamp { get; set; } = 30;

	private bool enabled = true;
	[DataMemberIgnore]
	public bool Enabled
	{
		get => enabled;
		set
		{
			enabled = value;
			character.SetVelocity(Vector3.Zero);
		}
	}

	private Vector3 currentVelocity;

	private CharacterComponent character;

	[DataMemberIgnore]
	public double NoiseAmount => currentVelocity.Length() / (Speed * RunFactor);

	public override void Start()
	{
		character = Entity.Get<CharacterComponent>();
		PitchClamp = MathUtil.DegreesToRadians(PitchClamp);
	}

	public override void Update()
	{
		if (!Enabled)
			return;
		Rotate();
		Move();
	}

	private void Move()
	{
		var inputDir = new Vector2();
		if (Input.IsKeyDown(Keys.A))
			inputDir.X -= 1;
		if (Input.IsKeyDown(Keys.D))
			inputDir.X += 1;
		if (Input.IsKeyDown(Keys.W))
			inputDir.Y += 1;
		if (Input.IsKeyDown(Keys.S))
			inputDir.Y -= 1;

		if (inputDir.LengthSquared() > 0.01)
			inputDir.Normalize();

		var moveSpeed = Speed * (Input.IsKeyDown(Keys.LeftShift) ? RunFactor : 1);
		var moveDir = Utils.LogicDirectionToWorldDirection(inputDir, Camera, Vector3.UnitY);
		var targetVelocity = moveDir * moveSpeed;
		currentVelocity = Vector3.Lerp(currentVelocity, targetVelocity, Acceleration);
		character.SetVelocity(currentVelocity);
	}

	private void Rotate()
	{
		CameraRootYaw.Rotation *= Quaternion.RotationY(-Input.MouseDelta.X);

		var cameraPitch = CameraRootPith.RotationEulerXYZ.X;
		var cameraPitchTarget = MathUtil.Clamp(cameraPitch - Input.MouseDelta.Y, -PitchClamp, PitchClamp);
		CameraRootPith.RotationEulerXYZ = new Vector3(cameraPitchTarget, 0, 0);
	}
}
}
