using Xenko.Engine;

namespace Glasses.UI
{
public class Finish : StartupScript
{
	public UIComponent MainUIComponent;
	public Wakeness Wakeness;
	public QuestManager Quests;
	public Player Player;

	public UIPage Success;
	public UIPage Fail;

	private UIComponent uiComponent;

	public override void Start()
	{
		uiComponent = Entity.Get<UIComponent>();
		Wakeness.OnWokeUp += () => ShowUI(Fail);
		Quests.OnQuestsCompleted += () => ShowUI(Success);
	}

	private void ShowUI(UIPage page)
	{
		Input.UnlockMousePosition();
		Game.IsMouseVisible = true;
		
		Player.Enabled = false;
		
		MainUIComponent.Enabled = false;
		uiComponent.Page = page;
		uiComponent.Enabled = true;
	}
}
}
