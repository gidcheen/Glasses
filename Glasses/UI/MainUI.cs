using Xenko.Engine;
using Xenko.UI.Controls;

namespace Glasses.UI
{
public class Main : SyncScript
{
	public Noise Noise { get; set; }
	public Wakeness Wakeness { get; set; }
	
	private Slider noiseSlider;
	private Slider wakenessSlider;

	public override void Start()
	{
		var uiPage = Entity.Get<UIComponent>().Page;
		noiseSlider = uiPage.RootElement.FindName("Noise") as Slider;
		wakenessSlider = uiPage.RootElement.FindName("Wakeness") as Slider;
	}

	public override void Update()
	{
		noiseSlider.Value = (float)Noise.Value;
		wakenessSlider.Value = (float)Wakeness.Value;
	}
}
}
