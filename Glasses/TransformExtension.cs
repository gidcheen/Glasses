using Xenko.Core.Mathematics;
using Xenko.Engine;

namespace Glasses
{
public static class TransformExtension
{
	public static (Vector3 position, Quaternion rotation, Vector3 scale) Global(this TransformComponent transform)
	{
		transform.UpdateWorldMatrix();
		transform.GetWorldTransformation(out var pos, out var rot, out var scale);
		return (pos, rot, scale);
	}

	public static Vector3 GetGlobalPosition(this TransformComponent transform) => transform.Global().position;
	public static Quaternion GetGlobalRotation(this TransformComponent transform) => transform.Global().rotation;
	public static Vector3 GetGlobalScale(this TransformComponent transform) => transform.Global().scale;
}
}
