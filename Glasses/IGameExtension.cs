using Xenko.Games;

namespace Glasses
{
public static class GameExtension
{
	public static double DeltaTime(this IGame game) => game.UpdateTime.Elapsed.TotalSeconds;
}
}
