using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Xenko.Audio;
using Xenko.Engine;

namespace Glasses
{
public class NoiseEmitter : SyncScript
{
	public PhysicsComponent Trigger;
	public Player Player;
	public Noise Noise;
	public double noiseTime;

	public NoiseAmount NoiseAmount = new NoiseAmount();

	public AudioEmitterComponent AudioEmitter;
	private AudioEmitterSoundController sound;

	private List<IEnumerator> coroutines = new List<IEnumerator>();

	public override void Start()
	{
		sound = AudioEmitter["Sound"];
		sound.IsLooping = false;

		Trigger.Collisions.CollectionChanged += (sender, args) =>
		{
			if (args.Action != NotifyCollectionChangedAction.Add)
				return;

			IEnumerator co = null;
			co = CollisionEntered();
			coroutines.Add(co);

			IEnumerator CollisionEntered()
			{
				sound.Play();
				Noise.Noises.Add(NoiseAmount);
				var time = 0.0;
				while (time < noiseTime)
				{
					time += Game.DeltaTime();
					yield return null;
				}
				Noise.Noises.Remove(NoiseAmount);
				coroutines.Remove(co);
			}
		};
	}

	public override void Update()
	{
		foreach (var coroutine in coroutines.ToList())
			coroutine.MoveNext();
	}
}
}
