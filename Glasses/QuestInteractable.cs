using Xenko.Engine;

namespace Glasses
{
public class QuestInteractable : StartupScript, IInteraction
{
	public QuestManager QuestManager;
	public string Quest;
	public bool Destroy;
	public string InteractionName { get; set; } = "ITEM_NAME";

	public void Interact(Player player)
	{
		QuestManager.FinishQuest(Quest);

		if (Destroy)
			Entity.EntityManager.Remove(Entity);
		else
			Entity.Remove(this);
	}
}
}
