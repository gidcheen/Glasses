using System;
using Xenko.Core.Mathematics;
using Xenko.Engine;

namespace Glasses
{
public class Wakeness : SyncScript
{
	private double value;
	public double Value
	{
		get => value;
		set
		{
			value = MathUtil.Clamp(value, 0, 1);
			if (Math.Abs(this.value - value) < float.Epsilon) 
				return;
			
			this.value = MathUtil.Clamp(value, 0, 1);
			if (this.value >= 1) 
				OnWokeUp?.Invoke();
		}
	}

	public double IncreaseTime = 1;
	public double IncreaseAmount = 0.02f;

	private float passedTime;

	public event Action OnWokeUp;

	public override void Update()
	{
		passedTime += (float)Game.DeltaTime();
		if (passedTime <= IncreaseTime)
			return;
		Value += IncreaseAmount;
		passedTime = 0;
	}
}
}
