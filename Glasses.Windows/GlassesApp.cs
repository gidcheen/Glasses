using Xenko.Engine;

namespace Glasses.Windows
{
internal static class GlassesApp
{
	private static void Main()
	{
		using (var game = new Game()) 
			game.Run();
	}
}
}
